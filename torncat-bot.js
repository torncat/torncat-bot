'use strict';
const date = require('date-and-time');
const Discord = require('discord.js');
const { env, prefix, token, watcherStartup } = require('./tcBot.json');
const logger = require('./lib/logger');
const util = require('./lib/util');
const broadcaster = require('./lib/broadcast');
const TornAPI = require('./lib/tornAPI');

// App data.
const now = new Date();
let botData = {
    start: date.format(now, 'YYYY/MM/DD HH:mm:ss', true),
    watchers: watcherStartup
};

// Discord client.
let client = new Discord.Client({ partials: ['MESSAGE', 'CHANNEL', 'REACTION'] });
client.commands = new Discord.Collection();

// Dynamically load bot commands.
util.reloadCommands(client);
let db = util.prepareDB();

client.once('ready', () => {
    // Prepare each sqlite table.
    for (let key of Object.keys(db)){
        let table = db[key];
        table.sync();
        logger.log('info', `Table ${table.name} synced.`);
    }
    let readyMsg = 'Bot restarted at ' + botData.start + ' on ' + env;
    broadcaster.broadcast(client, 'botStatus', readyMsg);
    logger.log('info', readyMsg);
    client.user.setUsername('TornCat Bot');
});

client.on('message', message => {
    // Replace Torn player URLs with a user badge embed.
    if (message.content.includes('torn.com/profiles.php?XID=') || message.content.includes('torn.com/loader.php?sid=attack&user2ID=')) {
        util.displayUserBadge(message);
        return;
    }

    // Only look for prefixed commands.
    if (!message.content.startsWith(prefix) || message.author.bot) return;
    const args = message.content.slice(prefix.length).split(/ +/);
    const commandName = args.shift().toLowerCase();

    if (commandName == 'stock'){
        saveNewStock();
        return;
    }

    // Unknown commandName.
    if (!client.commands.has(commandName)){
        message.reply('I don\'t know the command `' + commandName + '`');
        return;
    }

    const command = client.commands.get(commandName) || 
        client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));

    // Prevent DM access to most commands.
    if (command.guildOnly && message.channel.type !== 'text' && commandName != 'help') {
        message.reply('I can\'t execute `' + commandName + '` inside DMs!');
        return;
    }

    try {
        command.execute(message, args, botData);
    } catch (error) {
        logger.log('error', error);
        message.reply('there was an error trying to execute `' + commandName + '`');
    }
});

client.login(token);


/**
 *
 *
 *
 *
 *
 */


/**
 * Temporary function for testing saving stock data.
 * 
 * This function should eventually move to a better place.
 */
function saveNewStock(){
    const {discordServers} = require('./tcBot.json');
    const server = discordServers['698278893794361344'];
    const key = server.apiKeys[0];
    const stocks = new TornAPI.Stocks(key);
    stocks.callTorn('torn', '', 'timestamp,stocks')
        .then((data) => {
            for (let key of Object.keys(data.stocks)){
                let stock = data.stocks[key];
                stocks.saveToDB(key, stock, data.timestamp);
            }
        })
        .catch( );

}