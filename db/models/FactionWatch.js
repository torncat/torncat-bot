module.exports = (sequelize, DataTypes) => {
    return sequelize.define('factions', {
        id: {
            type: DataTypes.INTEGER,
            unique: true,
            allowNull: false,
            primaryKey: true,
        },
        name: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        },
        watch_enabled: {
            type: DataTypes.INTEGER,
            unique: false,
            allowNull: false,
            defaultValue: 1
        }
    },
    {
        timestamps: false,
        freezeTableName: true
    });
};