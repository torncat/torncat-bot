module.exports = (sequelize, DataTypes) => {
    return sequelize.define('players', {
        discord_id: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false,
        },
        torn_player_id: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        }
    });
};