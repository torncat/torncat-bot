module.exports = (sequelize, DataTypes) => {
    return sequelize.define('stocks', {
        id: {
            type: DataTypes.INTEGER,
            unique: true,
            allowNull: false,
            primaryKey: true,
        },
        acronym: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        },
        name: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        },
        current_price: {
            type: DataTypes.DOUBLE,
            unique: false,
            allowNull: false
        },
        total_shares: {
            type: DataTypes.DOUBLE,
            unique: false,
            allowNull: false
        },
        available_shares: {
            type: DataTypes.DOUBLE,
            unique: false,
            allowNull: false
        },
        forecast: {
            type: DataTypes.STRING,
            unique: false,
            allowNull: false
        },
        demand: {
            type: DataTypes.STRING,
            unique: false,
            allowNull: false
        },
        bonus_block: {
            type: DataTypes.REAL,
            unique: false,
            allowNull: true,
        },
        watch_enabled: {
            type: DataTypes.INTEGER,
            unique: false,
            allowNull: false,
            defaultValue: 0
        }
    },
    {
        timestamps: false,
    });
};