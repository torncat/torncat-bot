module.exports = (sequelize, DataTypes) => {
    return sequelize.define('stocks_history', {
        stock_id: {
            type: DataTypes.INTEGER,
            unique: false,
            allowNull: false,
        },
        current_price: {
            type: DataTypes.DOUBLE,
            unique: false,
            allowNull: false
        },
        total_shares: {
            type: DataTypes.DOUBLE,
            unique: false,
            allowNull: false
        },
        available_shares: {
            type: DataTypes.DOUBLE,
            unique: false,
            allowNull: false
        },
        forecast: {
            type: DataTypes.STRING,
            unique: false,
            allowNull: false
        },
        demand: {
            type: DataTypes.STRING,
            unique: false,
            allowNull: false
        },
        bonus_block: {
            type: DataTypes.REAL,
            unique: false,
            allowNull: true,
        },
        timestamp: {
            type: DataTypes.REAL,
            unique: false,
            allowNull: false,
        }
    },
    {
        timestamps: false,
        freezeTableName: true
    });
};