module.exports = (sequelize, DataTypes) => {
    return sequelize.define('itempricewatch', {
        id: {
            type: DataTypes.INTEGER,
            unique: true,
            allowNull: false,
            primaryKey: true,
        },
        name: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        },
        minimum_price: {
            type: DataTypes.INTEGER,
            unique: false,
            allowNull: false
        },
        image_url: {
            type: DataTypes.STRING,
            unique: false,
            allowNull: false
        }
    });
};