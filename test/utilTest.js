/**
 * Tests for util.js utility module.
 */

const {describe, it} = require('mocha');
const {expect} = require('chai');
const util = require('../lib/util');

describe('Util.formatNumber', function() {
    it('Million (10000001 -> 10,000,001)', function() {
        expect(util.formatNumber(10000001)).is.equal('10,000,001');
    });
    it('Thousand (10000 -> 10,000)', function() {
        expect(util.formatNumber(10000)).is.equal('10,000');
    });
    it('Decimals (10.0000 -> 10.0000)', function() {
        expect(util.formatNumber('10.0000')).is.equal('10.0000');
    });
    it('Mixed (12345.678901 -> 12,345.678901)', function() {
        expect(util.formatNumber(12345.678901)).is.equal('12,345.678901');
    });
});

describe('Util.rotateAPI: Check key rotations', function() {
    let server = {
        'apiKeys': [
            'apiKey1',
            'apiKey2',
            'apiKey3',
        ],
        apiKeyIndex: 0
    };
    it('First rotation.', function() {
        expect(util.rotateAPIKey(server)).is.equal('apiKey2');
    });
    it('Second rotation.', function() {
        expect(util.rotateAPIKey(server)).is.equal('apiKey3');
    });
    it('No rotation.', function() {
        expect(util.rotateAPIKey(server, false)).is.equal('apiKey3');
    });
    it('Rollover rotation.', function() {
        expect(util.rotateAPIKey(server)).is.equal('apiKey1');
    });

});