/**
 * Tests for TornAPI class and related functions.
 */

const {describe, it} = require('mocha');
const {expect} = require('chai');
const Torn = require('../lib/tornAPI');
const {discordServers} = require('../tcBot.json');
const sinon = require('sinon');

describe('TornAPI', function() {
    const server = discordServers['698278893794361344'];
    const key = server.apiKeys[0];
    let data = {};

    it('Torn is Down (stubbed)', async () => {
        const torn = new Torn.TornAPI(key);
        let callBentStub = sinon.stub(torn, 'callBent');
        callBentStub.returns({
            status: 500,
        });
        await torn.callTorn()
            .catch((error) => {
                data = error;
            });
        expect(data.error).to.equal('HTTP Response error (500)');
    });

    
    it('Key is empty', async () => {
        const torn = new Torn.TornAPI();
        await torn.callTorn()
            .catch((error) => {
                data = error;
            });
        expect(data.code).to.equal(2);
    });

    it('Incorrect Key', async () => {
        const torn = new Torn.TornAPI('asdf');
        await torn.callTorn()
            .catch((error) => {
                data = error;
            });
        expect(data).to.equal('Incorrect Key');
    });

    it('Basic user call - Duke', async () => {
        const torn = new Torn.TornAPI(key);
        await torn.callTorn('user', '4')
            .catch((error) => {
                data = error;
            });
        data = torn.getDATA();
        expect(data.name).to.equal('Duke');
    });

    it('Basic stocks call (full)- TCSE', async () => {
        const torn = new Torn.TornAPI(key);
        data = await torn.callTorn('torn', '0', 'timestamp,stocks')
            .catch((error) => {
                console.debug (error);
            });
        expect(data.stocks['0'].acronym).to.equal('TCSE');
    });

    it('Basic stocks call (stubbed)- TCSE', async () => {
        const torn = new Torn.TornAPI(key);
        let callBentStub = sinon.stub(torn, 'callBent');
        callBentStub.returns(
            {
                status: 200,
                json: () => {
                    return {
                        'timestamp':1589162859,
                        'stocks':{
                            '0':{'name':'Torn City Stock Exchange','acronym':'TCSE','director':'None','current_price':'10846.102','market_cap':0,'total_shares':0,'available_shares':0,'forecast':'Average','demand':'High'},
                        }
                    };
                }
            }
        );
        data = await torn.callTorn('torn', '0', 'timestamp,stocks')
            .catch((error) => {
                console.debug (error);
            });
        callBentStub.restore();
        expect(data.stocks['0'].acronym).to.equal('TCSE');
    });
});