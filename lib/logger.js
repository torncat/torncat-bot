/**
 * Sets the global logger.
 */

'use strict';

const {transports, format} = require('winston');
const { combine, timestamp, printf } = format;
const winston = require('winston');
require('winston-daily-rotate-file');

// Activity log file.
var transport = new (winston.transports.DailyRotateFile)({
    datePattern: 'YYYY-MM-DD',
    filename: 'logs/bot-%DATE%.log',
    maxFiles: '31d',
    maxSize: '20m',
    zippedArchive: true
});


const myFormat = printf(({ level, message, timestamp }) => {
    return `${timestamp} [${level}] ${message}`;
});

const logger = winston.createLogger({
    format: combine(
        timestamp(),
        myFormat
    ),
    transports: [
        new transports.Console(),
        transport
    ]
});

module.exports = logger;
