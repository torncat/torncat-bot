class TornAPI
{
    constructor(apiKey){
        this.data = {};
        this.key = apiKey;
        this.url = 'https://torn-proxy.com/';
    }

    callTorn(type, id = '', selections = '', skipCache = true){
        return new Promise((resolve, reject) => {
            if (skipCache == true){
                this.newAPIRequest(type, id, selections)
                    .then((data) => {
                        resolve(data);
                    })
                    .catch((err) => {
                        reject(err);
                    });

            } else {
                reject({});
            }
        });
    }

    cacheHit(type, selections){
        const now = (new Date).getTime();
        // Call db for related data.
        switch (type + '-' + selections){

        case 'user':
        default :
            return false;

        }
    }

    newAPIRequest(type, id = '', selections = ''){
        return new Promise((resolve, reject ) => {
            setTimeout(async () => {

                let streamURL = type + '/' + id + '?selections=' + selections + '&key=' + this.key;

                // Reject if key isn't set.
                if (this.key == undefined){
                    let error = {
                        code: 1,
                        error: 'Key is empty'
                    };
                    reject(error);
                }

                // Check HTTP status first to make sure API is actually up.
                let stream = await this.callBent(streamURL);
                if (stream.status != 200){
                    let error = {
                        code: 0,
                        error: 'HTTP Response error (' + stream.status + ')'
                    };
                    reject(error);
                } else {
                    // Check for Torn API error codes.
                    const result = await stream.json();
                    if (result.error != undefined){
                        reject(result.error);
                    } else {
                        this.data = result;
                        resolve(result);
                    }
                }
            }, 500);
        });
    }

    async callBent(url){
        const bent = require('bent');
        const getStream = bent(this.url);
        return await getStream(url);
    }

    getDATA(){
        return this.data;
    }
}

class Stocks extends TornAPI
{
    super(apiKey){
        this.data = {};
        this.key = apiKey;
        this.url = 'https://api.torn.com/';
    }
    saveToDB(stock_id, data, timestamp)  {
        const Sequelize = require('sequelize');
        // Load sqlite3 db
        const dbSettings = require('../db/database.json');
        const sequelize = new Sequelize('database', 'user', 'password', dbSettings);
        const StocksHistory = sequelize.import('../db/models/StocksHistory');
        data.createdAt = '';
        data.updatedAt = '';
        data.stock_id = stock_id;
        data.timestamp = timestamp;
        StocksHistory.create(data)
            .then(
                console.debug('Saving ' + data.name)
            )
            .catch((err) => {
                console.debug(err);
            });
    }

}

module.exports = {
    TornAPI,
    Stocks
};
