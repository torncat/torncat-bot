'use strict';
module.exports = {
    broadcast: function(client, channelType, msg){
        const {discordServers} = require('../tcBot.json');
        const logger = require('./logger');
        for (let key of Object.keys(discordServers)){
            if (key == '698278893794361344'){
                let server = discordServers[key];
                const channel = client.channels.cache.get(server.channels[channelType]);
                if (typeof msg == 'string'){
                    channel.send(msg)
                        .catch(err =>{logger.log('error', 'Attempted to send broadcast to ' + server.channels[channelType] + '\n' + err);});
                } else if (typeof msg == Object) {
                    channel.send({ embed: msg });
                }
            }
        }
    }
};