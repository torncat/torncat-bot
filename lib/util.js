'use strict';
module.exports = {
    checkPermissions: function(message, adminRoles){
        if (!message.guild || adminRoles.length == 0) return false;
        let permit = false;
        adminRoles.forEach(roleCheck => {
            if (message.member.roles.cache.find(r => r.name === roleCheck)){
                permit = true;
            }
        });
        if (!permit){
            message.reply('You don\'t have the correct role to run this command');
        }
        return permit;
    },
    displayUserBadge: function (message) {
        const logger = require('./logger');
        const Torn = require('./tornAPI');
        const regex = /(ID=)(\d*)/;
        let {discordServers} = require('../jdBot.json');
        let found = message.content.match(regex);
        let player_id = Number(found[0].slice(3));

        // Make sure the URL in the message has a playerID after the 'XID=' and player_id isn't Duke or Leslie.
        if (!player_id || isNaN(player_id) || player_id == 4 || player_id == 15){
            return;
        }

        let key = this.rotateAPIKey(discordServers[message.guild.id]);
        let torn = new Torn.TornAPI(key);

        torn.callTorn('user', player_id, 'personalstats,basic,profile')
            .then((player) => {
                let actionText = '';

                // Change action text based on player status.
                switch (player.status.state) {
                case 'Okay':
                    actionText = '[Attack](https://www.torn.com/loader.php?sid=attack&user2ID=' + player.player_id + ')\n';
                    break;
                case 'Hospital':
                    actionText = '[Revive](https://www.torn.com/profiles.php?XID=' + player.player_id + ')\n';
                    break;
                case 'Jail':
                    actionText = '[Bust](https://www.torn.com/profiles.php?XID=' + player.player_id + ')\n';
                    break;
                }

                // Only display faction text if player belongs to a faction.
                let factionText = 'No Faction\n';
                if (player.faction) {
                    factionText = 'Faction: [' + player.faction.faction_name + '](https://www.torn.com/factions.php?step=profile&ID=' + player.faction_id + ')\n';
                }

                // Fix for missing xantaken property API bug if player hasn't taken a xanax before.
                if (player.personalstats.xantaken == undefined){
                    player.personalstats.xantaken = 0;
                }
                // Discord embed.
                let embedMessage = {
                    title: player.name + ' [' + player.player_id + ']',
                    url: 'https://www.torn.com/profiles.php?XID=' + player.player_id,
                    description: actionText + factionText,
                    fields: [
                        {
                            name: 'Level',
                            value: player.level,
                            inline: true,
                        },
                        {
                            name: 'Age',
                            value: player.age,
                            inline: true,
                        },
                        {
                            name: 'Xanax',
                            value: player.personalstats.xantaken,
                            inline: true,
                        },
                        {
                            name: 'Status',
                            value: player.status.description,
                            inline: false,
                        },
                        {
                            name: 'Networth',
                            value: '$' + this.formatNumber(player.personalstats.networth),
                            inline: false,
                        },
                        {
                            name: 'Last Action',
                            value: player.last_action.relative,
                            inline: false,
                        }
                    ]
                };
                message.channel.send({embed: embedMessage});
                message.delete();
            })
            .catch((err) => {
                logger.log('error', 'Failed to call Torn. ' + err);
            });
    },
    formatNumber: function(num) {
        var parts = num.toString().split('.');
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        return parts.join('.');
    },
    prepareDB: function(){
        // Create db table models.

        // Load sqlite3 db
        const Sequelize = require('sequelize');
        const dbSettings = require('../db/database.json');
        const sequelize = new Sequelize('database', 'user', 'password', dbSettings);

        return {
            ItemPriceWatch: sequelize.import('../db/models/ItemPriceWatch'),
            FactionWatch: sequelize.import('../db/models/FactionWatch'),
            Players: sequelize.import('../db/models/Players'),
            Stocks: sequelize.import('../db/models/Stocks'),
            StocksHistory: sequelize.import('../db/models/StocksHistory'),
        };
    },
    reloadCommands: function(client){
        // Dynamically load command files.
        const fs = require('fs');
        const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));
        for (const file of commandFiles) {
            const command = require(`../commands/${file}`);
            //command.data = botData;
            client.commands.set(command.name, command);
        }

    },
    rotateAPIKey: function (server, rotate = true) {


        // For logging purposes, returns the current key, not the next rotated.
        if (!rotate){
            return server.apiKeys[server.apiKeyIndex];
        }

        // Setup Rotating API keys;
        server.apiKeyIndex++;
        if (server.apiKeyIndex >= server.apiKeys.length) {
            server.apiKeyIndex = 0;
        }

        return server.apiKeys[server.apiKeyIndex];
    },
    shuffleString: function(str){
        return str.split('').sort(function(){return 0.5-Math.random();}).join('');
    },
    uptime: function(start){
        // Returns uptime of server.
        const date = require('date-and-time');
        const now = new Date();
        start = new Date(start);
        start = date.addHours(start, -4);
        return (date.subtract(now, start).toHours()).toFixed(3);
    }

};
