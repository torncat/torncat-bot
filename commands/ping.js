module.exports = {
    name: 'ping',
    description: 'Returns a message from the server as well as server uptime in hours.',
    usage: '`!ping`',
    execute(message, args, botData) {
        const util = require('../lib/util');
        let msg = 'Pong!\n';
        msg += 'Server has been up for ' + util.uptime(botData.start) + ' hours';
        message.channel.send(msg);
    },
};