var exports = module.exports = {
    name:  'torndel',
    description: 'Admin only command. Deletes the Discord account\'s link to a Torn ID',
    usage: '`!torndel @DISCORD_NAME`',
    restricted: true,
    guildOnly: true,
    execute : function(message, args) {
        if (!args.length) {
            return message.reply(`You need to use the format ${this.usage}`);
        }
        const logger = require('../lib/logger');
        const util = require('../lib/util');
        const {discordServers} = require('../tcBot.json');
        const server = discordServers[message.guild.id];

        // Check to make sure that author has correct roles.
        if (!util.checkPermissions(message, server.botAdminroles)){
            return;
        }
        logger.log('info', 'Deleting link here for ' + args[0] + '!');
        let discord_id = getUserFromMention(args[0]);
            
        find(message, discord_id).catch((err) => { console.error(err); }).then((player_id)=>{
            if (player_id){
                remove(message, discord_id).catch((err) => { console.error(err); });
                message.channel.send('Deleted link: Discord (' + args[0] + ') <-> (' + player_id + ') Torn');
                message.channel.send(args[0] + ', please run `!tornjoin <TORN_ID>` again');
                message.channel.send('If you need help, you can type `!help tornjoin`');
            }
            else {
                message.channel.send('No connected account found');
            }
        });
            
        

    }

};
exports.data = this.data;

function getUserFromMention(mention) {
    if (!mention) return;

    if (mention.startsWith('<@') && mention.endsWith('>')) {
        let discord_id = mention.slice(2, -1);
        if (discord_id.startsWith('!')) {
            discord_id = discord_id.slice(1);
        }
        return discord_id;
    }
}

async function find(message, discord_id){
    const Sequelize = require('sequelize');

    // connect to db
    const dbSettings = require('../db/database.json');
    const sequelize = new Sequelize('database', 'user', 'password', dbSettings);
    let Players = sequelize.import('../db/models/Players');

    // See if player exists.
    // Select * from players where discord_id = 'discord_id' Limit 1;
    var player = await Players.findOne({ where: { discord_id: discord_id } }).catch((err) => { console.error(err); });
    if (player) {
        return player.get('torn_player_id');
    }
    return false;
}

async function remove(message, discord_id){
    const Sequelize = require('sequelize');

    // connect to db
    const dbSettings = require('../db/database.json');
    const sequelize = new Sequelize('database', 'user', 'password', dbSettings);
    let Players = sequelize.import('../db/models/Players');

    // Delete player.
    // Delete from players where discord_id = 'discord_id'
    await Players.destroy({ where: { discord_id: discord_id } }).catch((err) => { console.error(err); });
}