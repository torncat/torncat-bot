module.exports = {
    name: 'prune',
    description: 'Prune up to 100 messages.',
    usage: '`!prune <#>`   where # is number of messages to prune from current channel. Defaults to 10 if not provided.',
    restricted: true,
    guildOnly: true,
    examples: [
        '`!prune` will prune the last 10 messages',
        '`!prune 15` will prune the last 15 messages',
        '`!prune 50` will prune the last 50 messages',
    ],
    execute(message, args) {
        const logger = require('../lib/logger');
        const {discordServers} = require('../jdBot.json');
        const util = require('../lib/util');

        const server = discordServers[message.guild.id];

        // Check to make sure that author has correct roles.
        if (!util.checkPermissions(message, server.botAdminroles)){
            return;
        }

        let amount = parseInt(args[0]);

        if (isNaN(amount)) {
            amount = 10;
        }
        if (amount <= 1 || amount > 100) {
            return message.reply('You need to input a number between 1 and 99.');
        }

        message.reply('Are you sure you wish to delete ' + amount + ' messages? (Y/N)').then(() => {
            const filter = m => message.author.id === m.author.id;

            message.channel.awaitMessages(filter, { time: 12000, max: 1, errors: ['time'] })
                .then(messages => {
                    let response = messages.first().content;
                    switch (response) {
                    case 'y':
                    case 'Y':
                    case 'yes':
                    case 'Yes':
                        message.channel.bulkDelete(amount)
                            .then(
                                message.channel.send(amount + ' messages were deleted')
                            )
                            .catch(error => {
                                logger.log('error', 'Error during prune\n' + error);
                                message.channel.send('There was an error trying to prune messages in this channel!\nI can only delete messages sent within the last two weeks');
                            });
                        break;
                    case 'n':
                    case 'N':
                    case 'no':
                    case 'No':
                        message.reply('No messages deleted');
                        break;
                    default:
                        message.reply('I didn\'t understand.  Please retry the `!prune` command.');
                    }

                })
                .catch(() => {
                    message.reply('You did not enter any input.  Please retry the `!prune` command.');
                });
        });



    },
};