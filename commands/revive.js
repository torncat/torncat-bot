module.exports = {
    name:  'revive',
    description: 'Sends a revive request to the server\'s revive channel',
    usage: '!revive @DISCORD_NAME',
    examples: [
        '`!revive` will send a revive request for your Torn profile',
        '`!revive @DISCORD_NAME` will send a revive request for @DISCORD_NAME\'s Torn profile',
    ],
    guildOnly: true,
    execute : function(message, args) {
        const logger = require('../lib/logger');
        const {discordServers} = require('../tcBot.json');
        let discord_id = message.author.id;

        let server = discordServers[message.guild.id];
        const channel = message.client.channels.cache.get(server.channels['revives']);

        switch (args.length){
        case 0:
            find(discord_id).catch((err) => { logger.log('error', err); }).then((player_id)=>{
                if (player_id){
                    if (server.reviversRole != undefined){
                        let mention = getRole(message, server.reviversRole);
                        channel.send(`We need a revive, ${mention}`);
                    }
                    channel.send('https://www.torn.com/profiles.php?XID=' + player_id);
                }
                else {
                    message.reply('No connected account found, ' + message.author + '\nPlease use `!tornjoin TORN_PLAYER_ID` and then try again.');
                }
            }).catch(err => {logger.log('error', err);});
            break;

        case 1:
            discord_id = getUserFromMention(args[0]);
            if (discord_id){
                find(discord_id).catch((err) => { logger.log('error', err); }).then((player_id)=>{
                    if (player_id){
                        if (server.reviversRole != undefined){
                            let mention = getRole(message, server.reviversRole);
                            channel.send(`We need a revive, ${mention}`);
                        }
                        channel.send('https://www.torn.com/profiles.php?XID=' + player_id);
                    }
                    else {
                        message.reply('No connected account found.\nPlease have them run `!tornjoin TORN_PLAYER_ID` first and then try again.');
                    }
                }).catch(err => {logger.log('error', err);});
            } else {
                message.channel.send('No connected account found');
            }
            break;

        default:
            message.channel.send('You need to use the format `!tornid TORN_PLAYER_ID`');
        }
    }
};

function getUserFromMention(mention) {
    if (!mention) return;

    if (mention.startsWith('<@') && mention.endsWith('>')) {
        let discord_id = mention.slice(2, -1);
        if (discord_id.startsWith('!')) {
            discord_id = discord_id.slice(1);
        }
        return discord_id;
    }
}

async function find(discord_id){
    const Sequelize = require('sequelize');
    const logger = require('../lib/logger');
    // connect to db
    const dbSettings = require('../db/database.json');
    const sequelize = new Sequelize('database', 'user', 'password', dbSettings);
    let Players = sequelize.import('../db/models/Players');

    // See if player exists.
    // Select * from players where discord_id = 'discord_id' Limit 1;
    var player = await Players.findOne({ where: { discord_id: discord_id } }).catch((err) => { logger.log('error' , err); });
    if (player) {
        return player.get('torn_player_id');
    }
    return false;
}

function getRole(message, roleName){
    let foundRole = message.guild.roles.cache.find(r => r.name === roleName);
    return foundRole;
}