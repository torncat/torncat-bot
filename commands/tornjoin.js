module.exports = {
    name:  'tornjoin',
    usage: '!tornjoin <player_id>',
    examples: [
        '`!tornjoin 4` if you were Duke[4]',
        '`!tornjoin 15` if you were Leslie [15]',
    ],
    description: 'Links Torn ID with your Discord ID',
    guildOnly: true,
    execute : function(message, args) {
        const logger = require('../lib/logger');
        if (!args.length || args.length > 1 || isNaN(args[0])) {
            message.channel.send(`You need to use the format ${this.usage}`);
            message.channel.send('For instance, if you were Duke [4], then you would type `!tornjoin 4`');
            return;
        }

        let torn_player_id = args[0];
        let discord_id = message.author.id;
        linked(message, torn_player_id, discord_id).catch((err) => { logger.log('error', err); });
    }
};

async function linked(message, torn_player_id, discord_id){
    const Sequelize = require('sequelize');
    const logger = require('../lib/logger');

    // Connect to db
    const dbSettings = require('../db/database.json');
    const sequelize = new Sequelize('database', 'user', 'password', dbSettings);
    const {Op} = require('sequelize');
    // Define tables.
    let Players = sequelize.import('../db/models/Players');

    // see if player exists
    let query = {
        where: {
            [Op.or]: {
                torn_player_id: torn_player_id,
                discord_id: discord_id
            }
        }
    };

    var player = await Players.findOne(query).catch((err) => { logger.log('error', err); });

    if (player) {
        message.channel.send('This Torn player is already linked to https://www.torn.com/profiles.php?XID=' + player.torn_player_id);
        message.channel.send('If this is incorrect, please have a Discord bot-admin run !torndel @YOUR_DISCORD_Name');
        return;
    } else {
        // Create record.
        const newPlayer = await Players.create({
            'discord_id': discord_id,
            'torn_player_id': torn_player_id,
        }).catch((err) => { console.error(err); });
        if (newPlayer){
            message.delete().catch(error => {console.debug(error);});
            let message1 = 'https://www.torn.com/profiles.php?XID=' + torn_player_id;
            if (process.env.NODE_ENV != 'dev') {
                message.channel.send(message1);
            }
            logger.log('info', 'Linked ' + message1);
        }
    }
}