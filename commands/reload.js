module.exports = {
    name: 'reload',
    description: 'Reloads a command',
    usage: '`!reload <command>`',
    examples: [
        '`!reload` will (re)load all commands, even new ones',
        '`!reload help` will reload the help command'
    ],
    args: true,
    guildOnly: true,
    execute(message, args) {
        const logger = require('../lib/logger');
        const util = require('../lib/util');
        const {env} = require('../tcBot.json');

        if (args[0] === undefined){
            util.reloadCommands(message.client);
            let $msg = 'Reloaded all commands';
            logger.log('info', $msg);
            message.reply($msg);
            return;
        }

        const commandName = args[0].toLowerCase();
        const command = message.client.commands.get(commandName)
			|| message.client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));

        if (!command) {
            return message.channel.send(`There is no command with name or alias \`${commandName}\`, ${message.author}!`);
        }
        let newCommandData = null;

        // Persist existing data.
        if (command.data) {
            newCommandData = command.data;
        }

        delete require.cache[require.resolve(`./${command.name}.js`)];

        try {
            let newCommand = '';
            newCommand = require(`./${command.name}.js`);
            if (newCommandData){
                newCommand.data = newCommandData;
            }
            message.client.commands.set(newCommand.name, newCommand);
        } catch (error) {
            console.log(error);
            return message.channel.send(`There was an error while reloading a command \`${command.name}\`:\n\`${error.message}\``);
        }
        message.channel.send(`Command \`${command.name}\` was reloaded on the ${env} server.`);
    }
};


