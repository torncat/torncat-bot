# torncat-bot

TornCAT Discord Bot
https://www.TornCAT.com


## Installation
* Install node/ npm .
* Install [nodemon](https://nodemon.io/) globally.
* Clone this repo to your server
* Copy `tcBot.example.json` to `tcBot.json`
* Edit the fields in `tcBot.json` to reflect your bot/ server information
* Run `npm install --production`
* Run `npm run start`